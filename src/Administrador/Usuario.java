package Administrador;

public class Usuario {
	
	 	private int idUsuario;
	    private String nombreUsuario;
	    private int nip;
	    private int numTarjeta;
	    private Double fondoInicial;
	
	    public int getIdUsuario() {
			return idUsuario;
		}
		public void setIdUsuario(int idUsuario) {
			this.idUsuario = idUsuario;
		}
		public String getNombreUsuario() {
			return nombreUsuario;
		}
		public void setNombreUsuario(String nombreUsuario) {
			this.nombreUsuario = nombreUsuario;
		}
		public int getNip() {
			return nip;
		}
		public void setNip(int nip) {
			this.nip = nip;
		}
		public int getNumTarjeta() {
			return numTarjeta;
		}
		public void setNumTarjeta(int numTarjeta) {
			this.numTarjeta = numTarjeta;
		}
		public Double getFondoInicial() {
			return fondoInicial;
		}
		public void setFondoInicial(Double fondoInicial) {
			this.fondoInicial = fondoInicial;
		}
		
		 @Override
		    public String toString() {
		        return "Usuario"
		        		+ ": \n" + "idUsuario=" + idUsuario + ",\n nombreUsuario=" + nombreUsuario+ ",\n"
		        			+ " numTarjeta=" + numTarjeta +", \n nip=" + nip + ",\n fondoInicial=" + fondoInicial ;
		    }
		 }
