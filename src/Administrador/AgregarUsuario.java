package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AgregarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;
	private JTextField txtNom;
	private JTextField txtTarjeta;
	private JTextField txtNip;
	private JTextField txtFondos;

//	private JTextField txtNombre;
//	private JTextField txtNumtarjeta;
//	private JTextField textPIN;
//	private JTextField textFondoInicial;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarUsuario frame = new AgregarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AgregarUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(21, 91, 86, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Numero de Tarjeta:");
		lblNewLabel_1.setBounds(21, 116, 121, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("NIP: ");
		lblNewLabel_2.setBounds(21, 141, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fondo Inicial:");
		lblNewLabel_3.setBounds(21, 166, 98, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("ID:");
		lblNewLabel_4.setBounds(21, 66, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		txtID = new JTextField();
		txtID.setBounds(149, 63, 86, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);
		
		txtNom = new JTextField();
		txtNom.setBounds(148, 88, 149, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		txtTarjeta = new JTextField();
		txtTarjeta.setBounds(148, 113, 149, 20);
		contentPane.add(txtTarjeta);
		txtTarjeta.setColumns(10);
		
		txtNip = new JTextField();
		txtNip.setBounds(149, 138, 98, 20);
		contentPane.add(txtNip);
		txtNip.setColumns(10);
		
		txtFondos = new JTextField();
		txtFondos.setBounds(149, 163, 114, 20);
		contentPane.add(txtFondos);
		txtFondos.setColumns(10);
		
		JButton btnGuardar = new JButton("Agregar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MetodosAdmin metodos=new  MetodosAdmin();
				Usuario usuarios =new Usuario();
				
							usuarios.setIdUsuario(Integer.parseInt(txtID.getText()));
							usuarios.setNombreUsuario(txtNom.getText());
							usuarios.setNumTarjeta(Integer.parseInt(txtTarjeta.getText()));
							usuarios.setNip(Integer.parseInt(txtNip.getText()));
							usuarios.setFondoInicial(Double.parseDouble(txtFondos.getText()));
							metodos.insertarRegistro(usuarios);
							
							txtID.setText("");
							txtNom.setText("");
							txtTarjeta.setText("");
							txtNip.setText("");
							txtFondos.setText("");
							
							JOptionPane.showMessageDialog(null, "Los datos guardados son: \n "+ usuarios, getTitle(), JOptionPane.WARNING_MESSAGE);
				
			}
		});
		btnGuardar.setBounds(149, 211, 89, 23);
		contentPane.add(btnGuardar);
		
		JLabel lblNewLabel_5 = new JLabel("CREAR NUEVO USUARIO");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_5.setBounds(31, 11, 328, 33);
		contentPane.add(lblNewLabel_5);
		
		JButton btnRegresar = new JButton("Atras");
		btnRegresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				PrincipalAdmin principal = new PrincipalAdmin();
				principal.setVisible(true);
				
			}
		});
		btnRegresar.setBounds(244, 211, 89, 23);
		contentPane.add(btnRegresar);
	}

}
