package Administrador;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class MetodosAdmin {

	private ObjectContainer db=null;
	 
	 private void abrirRegistro() {
		 
		 //Archivo donde se guarda la BD
		 //abrirConexion
		 
	 db=Db4oEmbedded.openFile("registroUsuario");
			 }
	 
	 private void cerrarRegistro() {
	//Cierre de la conecion a la BD
		 db.close();
		 
	 }
	 
	 

	 

	public void insertarRegistro(Usuario usuario) {

        abrirRegistro();
		//db.store  Inserta Usuario en la BD
        db.store(usuario);
		cerrarRegistro();
		
	}

	public List<Usuario> seleccionarUsuarios(){
		abrirRegistro();
		ObjectSet listaUsuarios=db.queryByExample(Usuario.class);
		List<Usuario> lp =new ArrayList<>();
		for (Object listaUsuarios1:listaUsuarios) {
			lp.add((Usuario)listaUsuarios1);
			
		}
		cerrarRegistro();
		return lp;
		
	}

	//Metodo de Consulta
	
	public Usuario seleccionarUsuario(Usuario usuarios) {
		abrirRegistro();
		ObjectSet resultado=db.queryByExample(usuarios);
		Usuario usuario=(Usuario) resultado.next();
		cerrarRegistro();
		return usuario;
		
	}
	//Metodo de Update

	public void actualizarRegistro(int id,String nombre,Double fondoInicial,int numTarjeta, int nip) {

		abrirRegistro();
		Usuario p = new Usuario();
		p.setNumTarjeta(numTarjeta);
		ObjectSet resultado=db.queryByExample(p);
		Usuario auxiliar=(Usuario) resultado.next();
		auxiliar.setIdUsuario(id);
		auxiliar.setNombreUsuario(nombre);
		auxiliar.setFondoInicial(fondoInicial);
		auxiliar.setNumTarjeta(numTarjeta);
		auxiliar.setNip(nip);
		db.store(auxiliar);
		cerrarRegistro();
	}

	public Usuario eliminarRegistros(int id) {
		abrirRegistro();
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(id);
		ObjectSet resultado=db.queryByExample(usuario);
		Usuario auxiliar=(Usuario) resultado.next();
		//Metodo de eliminar db.delete
		db.delete(auxiliar);
		cerrarRegistro();
		return auxiliar;
		
		
	}

	
	
}
