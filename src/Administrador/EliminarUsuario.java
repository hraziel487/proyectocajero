package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EliminarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarUsuario frame = new EliminarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("IDUsuario");
		lblNewLabel.setBounds(21, 69, 84, 14);
		contentPane.add(lblNewLabel);
		
		txtID = new JTextField();
		txtID.setBounds(81, 66, 86, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				
				MetodosAdmin metodos=new  MetodosAdmin();
				Usuario usuario=new Usuario();
				int id;

				id=(Integer.parseInt(txtID.getText()));
				usuario = metodos.eliminarRegistros(id);
				
				JOptionPane.showMessageDialog(null, "Los datos seleccionados eliminados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);
				
				
			
				
			}
		});
		btnEliminar.setBounds(177, 65, 89, 23);
		contentPane.add(btnEliminar);
		
		JButton btnregresar = new JButton("Atras");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				PrincipalAdmin principal = new PrincipalAdmin();
				principal.setVisible(true);
			
			}
		});
		btnregresar.setBounds(276, 65, 89, 23);
		contentPane.add(btnregresar);
	}

}
